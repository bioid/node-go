# Use an existing docker image as a base
FROM ubuntu:16.04
# install stuff
RUN apt-get clean && apt-get update
RUN apt-get install -y apt-utils build-essential cmake 
RUN apt-get install -y golang-1.6 golang-1.6-go
RUN apt-get install -y curl 
RUN ln -s /usr/lib/go-1.6/bin/go /bin/go
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get -y install nodejs

# copy go/c src
WORKDIR /usr/gosharedlib/go
COPY ./go .

# build the c shared library 
RUN ["bash", "build.sh"]

# copy node package.json first and install
WORKDIR /usr/gosharedlib/node
COPY ./node/package.json .
RUN npm install

# copy node src
WORKDIR /usr/gosharedlib/node
COPY ./node .

# start node
CMD ["npm", "start"]
#CMD ["npm", "run", "debug"]
#CMD ["/bin/bash"]