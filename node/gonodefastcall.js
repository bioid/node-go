let ref = require('ref');
let fastcall = require('fastcall');
let StructType = fastcall.StructType;
let Library = fastcall.Library;

let lib = new Library('../go/build/gohttplib.so')

let req = {

}

let RequestStruct = new StructType(req)

lib.struct({
        Request: {
            'Method': ref.types.CString,
            'Host': ref.types.CString,
            'URL': ref.types.CString,
            'Body': ref.types.CString,
            'Headers': ref.types.CString
        }
    })
    .asyncFunction({'ListenAndServe': ['void', ['char *']]})
    //extern int ResponseWriter_Write(unsigned int p0, char* p1, int p2);
    .function({'ResponseWriter_Write': ['int', ['uint', 'char *', 'int']]})
    //extern void ResponseWriter_WriteHeader(unsigned int p0, int p1);
    .function({'ResponseWriter_WriteHeader': ['void', ['uint', 'int']]})
    .callback({'cb': ['void', ['uint', 'Request *']]})
    .function({'HandleFunc': ['void', ['char *', 'cb']]})
  
module.exports = { lib: lib.interface, structs: lib.structs };