let { lib, structs } = require('./gonodefastcall');

let fastcall = require('fastcall');
let ref = require('ref');
let handlers = [];

class ResponseWriter {
    constructor(w) {
        this._w = w;
    }
    write(body) {
        let n = lib.ResponseWriter_Write(this._w, fastcall.makeStringBuffer(body), body.length);
        if (n !== body.length)
            throw Error('Failed to write to ResponseWriter');
    }
    setStatus(code) {
        lib.ResponseWriter_WriteHeader(this._w, code);
    }
}

class Request {
    constructor(reqPtr) {
        //let req = {};
        let { Request } = structs;
        this._req = {};
        let body = Request._body;
        //console.log(Request)
        let fields = Object.keys(body);
        for (let i = 0; i < fields.length; i++) {
            let field = fields[i];
            let metaData = Request._body[field];
            let type = ref.types[metaData.name];
            let fieldPtr = ref.reinterpret(reqPtr, type.size);
            fieldPtr.type = type;
            this._req[field] = fieldPtr.deref();
            let nextSize = 0;
            if (i + 1 < fields.length) {
                let nextType = Request._body[fields[i+1]].name;
                nextSize = ref.types[nextType].size;
            }
            reqPtr = ref.reinterpret(reqPtr, nextSize, type.size)
        }
    }
    get method() {
        return this._req.Method;
    }

    get host() {
        return this._req.Host;
    }

    get url() {
        return this._req.URL;
    }

    get body() {
        return this._req.Body;
    }

    get headers() {
        return this._req.Headers;
    }
}

function Route(pattern, fn) {
    let handler = lib.cb((w, req) => fn(new Request(req), new ResponseWriter(w)));
    handlers.push(handler);
    lib.HandleFunc(fastcall.makeStringBuffer(pattern), handler);
}

function run(host='0.0.0.0', port=8080) {
    let bind = `${host || ''}:${port}`;
    console.log(`* Running on http://${bind}/`);
    lib.ListenAndServe(fastcall.makeStringBuffer(bind));
}

module.exports = {
    ResponseWriter, Request, Route, run, 
}